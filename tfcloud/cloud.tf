terraform {
  cloud {
    hostname = "app.terraform.io"
    organization = "uspto"

    workspaces {
      name = "my-example"
    }
  }
}
